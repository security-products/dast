## DAST

This project contains released images for a GitLab security analyzer and was configured as per https://gitlab.com/gitlab-org/gitlab/-/issues/330671 and https://gitlab.com/gitlab-org/gitlab/-/issues/297525#group-and-project-settings.

### Relevant links

- Source code: https://gitlab.com/gitlab-org/security-products/dast
- Feature documentation: https://docs.gitlab.com/ee/user/application_security/dast/
- Images: https://gitlab.com/security-products/dast/container_registry
